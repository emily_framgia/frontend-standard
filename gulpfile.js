'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

var runSequence = require('run-sequence');
var browserSync = require('browser-sync').create();
var del = require('del');
var pug = require('pug');
var fs = require('fs');

var
  source = 'source/',
  dest = 'dest/';

var options = {
  del: [
    'dest'
  ],
  browserSync: {
    server: {
      baseDir: dest
    }
  },
  htmlPrettify: {
    'indent_size': 2,
    'unformatted': ['pre', 'code'],
    'indent_with_tabs': false,
    'preserve_newlines': true,
    'brace_style': 'expand',
    'end_with_newline': true
  },

  pug: {
    pug: pug,
    pretty: '\t'
  },
  include: {
    hardFail: true,
    includePaths: [
      __dirname + "/",
      __dirname + "/node_modules",
      __dirname + "/source/js"
    ]
  },
}

var scss = {
  sassOpts: {
    outputStyle: 'nested',
    precison: 3,
    errLogToConsole: true,
    includePaths: [
      './node_modules/bootstrap-sass/assets/stylesheets',
      './node_modules/font-awesome/scss/'
    ]
  }
};


// fonts
var fonts = {
  in: [
    source + 'fonts/*.*',
    './node_modules/bootstrap-sass/assets/fonts/*',
    './node_modules/font-awesome/fonts/*'
  ],
  out: dest + 'fonts/'
};

// js
var js = {
  in: [
    source + 'js/*.*',
    './node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js'
  ],
  out: dest + 'js/'
};

/**
 * Filter block:
 * Allow add filter
 *
 */
pug.filters.code = function( block ) {
  return block
    .replace( /&/g, '&amp;' )
    .replace( /</g, '&lt;' )
    .replace( />/g, '&gt;' )
    .replace( /"/g, '&quot;' );
}


/**
 * Tasks
 * Allow add filter
 *
 */
gulp.task('browser-sync', function() {
  return browserSync.init(options.browserSync);
});

gulp.task('watch', function (cb) {
  $.watch(source + '/sass/**/*.scss', function () {
    gulp.start('compile-styles');
  });

  $.watch([source + '/*.pug',source + '/**/*.pug'], function () {
    return runSequence('compile-pages', browserSync.reload);
  })

  $.watch(source + '/**/*.js', function () {
    return runSequence('compile-js', browserSync.reload);
  })
  $.watch(source + '/modules/*/data/*.json', function () {
    return runSequence('build-html', browserSync.reload);
  })
})

// copy js
gulp.task('js', function () {
  return gulp
    .src(js.in)
    .pipe(gulp.dest(js.out));
});

// copy font
gulp.task('fonts', function () {
  return gulp
    .src(fonts.in)
    .pipe(gulp.dest(fonts.out));
});

// = Delete
gulp.task('cleanup', function (cb) {
  return del(options.del, cb);
});


// = Main tasks
gulp.task('build', function (cb) {
  return runSequence(
    'cleanup',
    'compile-images',
    'compile-styles',
    'compile-js',
    'build-html',
    cb
    );
});

// = Build Style

gulp.task('compile-styles',['fonts'], function (cb) {
  return gulp.src([
    source + '/sass/*.scss',
    '!'+ source +'/sass/_*.scss'
  ])
  .pipe($.sourcemaps.init())
  .pipe($.sass(scss.sassOpts)
    .on('error', $.sass.logError))
  .pipe($.autoprefixer('last 2 versions'))
  .pipe($.concat('main.css'))
  .pipe($.sourcemaps.write('./', {
    includeContent: false,
    sourceRoot: source + '/sass'
  }))
  .pipe(gulp.dest(dest + '/css'))
  .pipe(browserSync.stream());
})

// = Build HTML

gulp.task('compile-pages', function (cb) {
  var jsonData = JSON.parse(fs.readFileSync('./tmp/data.json'));
  options.pug.locals = jsonData;
  return gulp.src(['*.pug', '!_*.pug'], {cwd: 'source'})
  .pipe($.pugInheritance({basedir: "source"}))
  .pipe($.pug(options.pug))
  .pipe($.prettify(options.htmlPrettify))
  .pipe(gulp.dest(dest));

})

gulp.task('build-html', function (cb) {
  return runSequence(
    'combine-data',
    'compile-pages',
    cb
  );
});


// = Build JS

gulp.task('compile-js', function() {
  return gulp.src(["*.js", "!_*.js"], {cwd: 'source/js'})
  .pipe($.include(options.include))
  .pipe(gulp.dest(dest + '/js'));
});


// = Build image
gulp.task('compile-images', function() {
  return gulp.src(source + "/images/*.*")
  .pipe($.jshint())
  .pipe($.jshint.reporter('default'))
  .pipe(gulp.dest(dest + '/images'));
});

// = Build DataJson
gulp.task('combine-modules-json', function (cb) {
  return gulp.src(['**/*.json', '!**/_*.json'], {cwd: 'source/modules/*/data'})
    .pipe($.mergeJson('data-json.json'))
    .pipe(gulp.dest('tmp/data'));
});

gulp.task('combine-modules-data', function (cb) {
  return gulp.src('**/*.json', {cwd: 'tmp/data'})
    .pipe($.mergeJson('data.json'))
    .pipe(gulp.dest('tmp'));
});

// Service tasks

gulp.task('combine-data', function (cb) {
  return runSequence(
    [
      'combine-modules-json'
    ],
    'combine-modules-data',
    cb
  );
});

// ================ Develop

gulp.task('dev', function (cb) {
  return runSequence(
    'build',
    [
    'browser-sync',
    'watch'
    ],
    cb
    )
})
